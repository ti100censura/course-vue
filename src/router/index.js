import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Cadastro from '../views/Cadastro.vue'
import Jobs from '../views/jobs/Jobs.vue'


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, 
  {
    path: '/cadastro-ds',
    name: 'Cadastro',
    component: Cadastro
  },       
  {
    path: '/jobs',
    name: 'Jobs',
    component: Jobs
  },      
 
  {
    path: '/jobs/:id',
    name: 'JobsId',
    component: Jobs
  },       
 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router